# Installation

installer les dépendances:

    bundle install


# Lancement

Lancer le serveur local avec:

    bundle exec jekyll serve


Le site sera disponible sur http://127.0.0.1:4000/


# Liens

- Documentation jekyll: https://jekyllrb.com/docs/installation/
