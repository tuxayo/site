--- 
published: true
title: Bilan de notre premier galop
layout: post
author: Ma Dada team
category: articles
tags: 
- madada
- actualité

---

### La plateforme Ma Dada

La plateforme Ma Dada permet d'effectuer et de faciliter les demandes d'accès à des documents administratifs. La liberté d'accès aux documents administratifs est **la possibilité pour tout citoyen d'obtenir la communication d'un document produit ou reçu par une administration publique** (par exemple un service de l'État, une collectivité territoriale ou un organisme  chargé d'une mission de service public) [source](https://www.service-public.fr/particuliers/vosdroits/F2467). 

Sur Ma Dada ces demandes sont facilitées par une interface qui recense les coordonnées des autorités administratives, qui permet d'effectuer des demandes, d'y obtenir les éventuelles réponses et documents associés, mais aussi d'être informé sur ses démarches et d'avoir un suivi détaillé de celles-ci.  

Ces demandes ainsi que les documents administratifs obtenus en réponse étant par défaut publics et consultables librement sur http://madada.fr, la plateforme joue également un rôle de diffusion libre et transparente de ces données.

### Bilan de la première année d'existence de Ma Dada

La plateforme Ma Dada est née suite à la découverte du site de son acolyte britannique, la plateforme [What Do They Know](https://www.whatdotheyknow.com/) qui compte aujourd'hui plus de 700 000 requêtes FOI (Freedom of information act), dont 400 000 ont ayant abouti à la communication des informations publiques demandées.

Ma Dada et What Do They Know sont toutes deux basées sur le logiciel libre [Alaveteli](https://alaveteli.org/) édité par la fondation l'association à but non lucratif [MySociety](https://www.mysociety.org/). Ce libriciel est ainsi déployé dans plus de [25 pays](https://alaveteli.org/deployments/) différents, selon les spécificités techniques ou administratives de chaque pays, l'objectif commun étant la facilitation des demandes d'accès à des documents administratifs. C'est ainsi qu'en Belgique cela donne [Transparencia.be](https://transparencia.be/), [Fragdenstaat.de](https://fragdenstaat.de/) en Allemagne ou encore [Informini](https://www.informini.org/) en Tunisie. 

La plateforme française Ma Dada, portée par l'association [Open Knowledge France](https://fr.okfn.org) a été lancée en octobre 2019. Un hackathon citoyen avait alors permis d'agréger la liste des autorités publiques françaises et les coordonnées email des personnes responsable de l'accès aux documents administratifs (PRADA) correspondantes. 

Après un an d'existence, Après un peu plus d'un an d'existence, la plateforme dénombre 455 demandes publiques d'accès à des documents administratifs. 

![bilan chiffré 2020](https://gitlab.com/madada-team/site/-/raw/site/_posts/images/stats2020.png?inline=false) 

### Un exemple d'usage de Ma Dada 

Parmi les succès de cette première année d'existence, citons la publication du [rapport « Sans Contreparties :  pour un revenu minimum garanti»](https://www.secours-catholique.org/sites/scinternet/files/publications/rapport_revenusans_contreparties.pdf) du Secours Catholique. Ce rapport préconise la mise en place d'un revenu minimum inconditionnel garanti  qui  préconise la mise en place d'un revenu minimum inconditionnel garanti, cartographie et analyse la situation des personnes bénédiciant du RSA en France, ainsi que les procédures de contrôle et de sanctions qui l'accompagnent. 

Le rapport s'appuie entre autres sur les données récoltées auprès des conseils départementaux en charge de la gestion du revenu de solidarité active (RSA), données obtenues via Ma Dada, afn de déterminer la proportion d’allocataires ayant fait l’objet d’une procédure de sanction ces dernières années. Ce rapport a depuis fait l'objet d'un [article (accès abonnés)](https://www.mediapart.fr/journal/france/021020/le-secours-catholique-milite-pour-un-rsa-sans-condition-ni-sanction) sur Mediapart. 

### Des progrès mais encore du chemin à parcourir

Sur 460 demandes effectuées à ce jour, 88 ont abouti à la communication du document administratif demandé. Le [patrimoine immobilier de la ville de Bordeaux](https://madada.fr/demande/patrimoine_immobilier), [les comptes administratifs de la région Haut-de-France](https://madada.fr/demande/comptes_administratifs_du_budget_3), le plan départemental des chemins et itinéraires de randonnées des départements de l'[Aisne](https://madada.fr/demande/pdipr_plan_departemental_des_iti_4) et de l'[Allier](https://madada.fr/demande/pdipr_plan_departemental_des_iti_5), la [base de données des réseaux routiers](https://madada.fr/demande/base_de_donnees_des_reseaux_rout) en font partie. 

En revanche, pour les données de subvention octroyées par les départements ou la liste de leurs marchés publics, **seulement 24 départements sur 97 ont transmis la liste de leurs marchés publics et 13 départements sur 97 ont transmis la liste de leurs subventions** alors que ces données ont fait l'objet de décrets spécifiques et que l'association Opendata France a proposé un format standard pour leur publication.

Il reste encore souvent nécessaire de convaincre du bien fondé de la demande formulée et de s'armer de patience et de ténacité afin d'exercer ce droit citoyen. Nous aurons l'occasion de revenir plus en détails sur ces aspects dans un prochain billet de blog. D'ici là n'hésitez pas à tester la plateforme en demandant à vos autorités publiques favorités n'importe quel document public qui vous intéresse.


