---
layout: page
title: Juridique
permalink: /juridique/
---

[accueil](/site/)
[about](/site/about/)
[juridique](/site/juridique/)
# INFOS JURIDIQUES

Infos juridiques récupérées sur [Site originel Wix](https://soutien13gj.wixsite.com/home/infos-juridiques-utiles)

## Pendant une manif :

* [filme un flic sauve une vie](https://mars-infos.org/filme-un-flic-sauve-une-vie-petit-3757)

Attention! Les policiers n'aiment pas être filmés. Aussi faites le à 2 avec une personne qui filme celui qui filme ;)

* [vos-droits-de-manifestant](https://www.giletau.org/vos-droits-de-manifestant/)


## Pendant une manif et GAV :

* [conseils-manif](http://gjinfo.org/conseils-manif/)

## Pendant une GAV :

* [garde-a-vue-avocats-gilets-jaunes](http://gjinfo.org/garde-a-vue-avocats-gilets-jaunes/)
* [nos-droit-en-garde-a-vue](http://gjinfo.org/nos-droit-en-garde-a-vue/)

## Après une GAV :

* [consultation-et-effacement-des-3882](https://mars-infos.org/consultation-et-effacement-des-3882)
* [contre-le-fichage-organisons-nous-3259](https://mars-infos.org/contre-le-fichage-organisons-nous-3259)

## Pour les prisonniers :

* [que-faire-quand-un-e-proche-est-3603](https://mars-infos.org/que-faire-quand-un-e-proche-est-3603)

## flyer droits

* [lt-mars.pdf](https://mars-infos.org/home/chroot_ml/ml-marseille/ml-marseille/public_html/IMG/pdf/lt-mars.pdf)
* [manifestantes-droits-et-conseils-en-cas-dinterpellation](http://lesaf.org/manifestantes-droits-et-conseils-en-cas-dinterpellation/)

## guide manifestant

* poche: [guide-de-poche-manifestant.pdf](https://lookaside.fbsbx.com/file/guide-de-poche-manifestant.pdf?token=AWw_jWM88JtFRsbVK6kuzp8us3xoWBxnSlq0oRyUcw3ay2CUFSe_8TjgYI1Y8P2qHvjq8rqT4JP_cdhMUON4HS47r4jsIrwuTyKvPdmKByr5NoCb14utBDNm4Ifh8S-ueZdQCFvIBm0W381hC2spmIDEnkdyye5FbRhPez5U67ZAcA)
* lois : [Guide%20pratique%20et%20d%C3%A9ontologique%20du%20gilet%20jaunes.pdf](https://lookaside.fbsbx.com/file/Guide%20pratique%20et%20d%C3%A9ontologique%20du%20gilet%20jaunes.pdf?token=AWwrmm70AgA0sjs9-mzAF1DVF153mXhuFW-XrDJfhFhzsdA8FlrXEM2zqniK5yH67wIJN4sdmiSS1nrpnK5dcHfq4LPKSrvjD3t5V8FBlMQNCV8SHzb25L-BbnK97tfvaEzeDd-ljmn0z97KeCbnQqCF81ZLTNspTuFgu-Q3XnD8MQ)
* complet : [Guide-complet-du-manifestant.pdf](https://lookaside.fbsbx.com/file/Guide-complet-du-manifestant.pdf?token=AWxZSYxiqt6okYmt2qE9u542RVUw1xyy_dheAq3t2FnzuGmzO2oKT9IrrI8Pbfroli8IS2Jo5HgX1Hhu_Kl14Q94a8OO82NUWFSLhljOOQ3Arl4vvgERPA2YdJTGrCYyVVMmuz-hh5nkHRme4ozdTA_kwCuKOeWmiQiPODaxfp8cQQ)

## Etat des lieux :

* [KIT%20BLESSURES%20PDF.pdf](https://lookaside.fbsbx.com/file/KIT%20BLESSURES%20PDF.pdf?token=AWwYkrObTCmnpBNDNAtGoimFV4NIM4pbkAdoCNM9KmrevMIMTvL1vZLmtMCPJ5qCOid0RFbKELH-6I-puBqk7xl7WBR2aPe-EWgclth3t2BmRba-NJtqAHDglZl5uusW_ZZXTC4f8L8XWpWoTgTdweKZ5eQL1lJs9UtOFpn3ZP6xfQ)

## Concevoir distribuer flyer
* [Législation distribution de tracts](https://www.giletau.org/wp-content/uploads/asgarosforum/140/l%c3%a9gislation-distribution-de-tracts.pdf)

## Vidéos de la super avocate d'avignon
*[droits des street medics, question sur l'évacuation des rond-point , question sur les amendes 135 euros pour port de GJ ou t-shirt RIC, manif interdites, dispersion après sommation, entrave à la circulation,rassemblement /attroupement différence et conséquences, ](https://www.youtube.com/watch?v=5QB3Ka2_rCw&feature=share)

* [évacuation terrain, interpellations ou empechement de manifester, droit de filmer](https://www.youtube.com/watch?v=Nmb7nZdo2vI)
